# Demosessies

Dit project bevat de slides van iedere demosessie met ingang van 9 maart 2022

| Datum        | Link
| ------------ | -------------------
| 9 maart 2022 | [Demosessie Portaal en NLX](./slides/20220309-Virtueel-Inkomsten-Loket-Demo.pdf)
| 20 april 2022 | [Demosessie Portaal, Gemeenten en Samenwerking](./slides/20220420%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 1 juni 2022 | [Demosessie leerervaringen pilot](./slides/20220601%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 20 juli 2022 | [Demosessie kamerbrief](./slides/202207020%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 7 sep 2022 | [Demosessie NLX sessies uitvoeringsorg.](./slides/202209007%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 12 okt 2022 | [Demosessie SZW: Topberaad Inkomensondersteuning](./slides/20221012%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 23 nov 2022 | [Demosessie plateaus én regels.overheid.nl](./slides/20221123%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 18 jan 2023 | [Demosessie plateaus én IGS & Doccle demo](./slides/20230118%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 1 mrt 2023 | [Demosessie Ontwikkeling in plateau’s](./slides/20230301%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 19 apr 2023 | [IGS Pilot & Solid demo](./slides/20230419%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 31 mei 2023 | [Landelijke ontwikkelingen](./slides/20230531%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
| 5 juli 2023 | [VIL On Hold](./slides/20230705%20Virtueel%20Inkomsten%20Loket%20Demo.pdf)
